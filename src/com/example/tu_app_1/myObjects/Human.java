package com.example.tu_app_1.myObjects;

import android.os.Parcel;
import android.os.Parcelable;

public class Human implements Parcelable {
	public static String name, address;

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 555;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(name);
		dest.writeString(address);
	}

	public Human(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Human createFromParcel(Parcel in) {
			String name = Human.name;
			String address = Human.address;
			return new Human(name,address);
		}

		public Human[] newArray(int size) {
			return new Human[size];
		}
	};
}
