package com.example.tu_app_1;

import java.util.ArrayList;

import com.example.tu_app_1.myObjects.Human;
import com.example.tu_app_1.myadapter.Adapter1;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.ListView;

public class secondActivity extends Activity {
	ArrayList<String> arr;
	Adapter1 customadapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listviewlayout);
		
		
		Bundle extra = getIntent().getExtras();
		Human human = extra.getParcelable("human1");
		
		MainActivity.showmsg(this, human.name);
		
		arr = new ArrayList<String>();
		arr.add(human.name);
		arr.add(human.address);
		
		
		customadapter = new Adapter1(this, arr);
		
		ListView listv = (ListView) findViewById(R.id.listview1);
		listv.setAdapter(customadapter);
//		
		
		
		
	}

}
