package com.example.tu_app_1;

import com.example.tu_app_1.myObjects.Human;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter.LengthFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		View Btn1 = findViewById(R.id.button1);
		View Btn2 = findViewById(R.id.button2);
		View Btn3 = findViewById(R.id.button3);
		View Btn4 = findViewById(R.id.button4);
		Btn1.setOnClickListener(this);
		Btn2.setOnClickListener(this);
		Btn3.setOnClickListener(this);
		Btn4.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {

		case R.id.button1:
			Log.w("myApp", "show second activity click");
			// intent = new Intent(this, secondActivity.class);
			Human human = new Human("ty", "123 duong so 1");
			intent = new Intent(this, secondActivity.class);
			intent.putExtra("human1", human);

			startActivity(intent);
			break;

		case R.id.button2:
			Log.w("myApp", "drawable layout");
			showmsg(this, "show drawable clicked");
			intent = new Intent(this, mydrawable.class);
			startActivity(intent);
			break;

		case R.id.button3:
			Log.w("myApp", "wifi check");

			// check wifi here!

			ConnectivityManager connectivityManager = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (mWifi.isConnected()) {
				showmsg(this, "wifi is connected!");
			} else {
				showmsg(this, "NO wifi connection!");
			}

			// intent = new Intent(this, mydrawable.class);
			// startActivity(intent);
			break;

		case R.id.button4:
			Log.w("myApp", "scroll view login click");
			// create a new intent
			intent = new Intent(this, Activity_scrollview_login.class);
			
			// start intent
			startActivity(intent);
			break;

		default:
			break;
		}

	}

	public static void showmsg(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
}
