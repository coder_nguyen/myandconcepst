package com.example.tu_app_1;

import android.R.string;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public final class Activity_scrollview_login extends Activity{
	public static final String ref_file_name = "mySharedRef";
	EditText editext_name;
	EditText editext_pass;
	SharedPreferences shareref;
	Context context = this;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scrollviewlayout);
		
		editext_name = (EditText) findViewById(R.id.editText1);
		editext_pass = (EditText) findViewById(R.id.editText1);
		
		Button btnlogin = (Button) findViewById(R.id.btnlogin);
		
		shareref = getSharedPreferences(ref_file_name, Context.MODE_PRIVATE);
		
		TextView tv = (TextView)findViewById(R.id.tvlogin);
		tv.setText(shareref.getString("username","000"));
		
		btnlogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.w("myApp", "scroll view login click");
				
				
				//save 
				Editor edit = shareref.edit();
				edit.putString("username", editext_name.getText().toString());
				edit.putString("pass", editext_pass.getText().toString());
				edit.commit();
				
				String st = "input info: " + shareref.getString("username","000") 
						+ shareref.getString("username","000");
				Log.w("myApp", st);
				
				Toast.makeText(context,st, Toast.LENGTH_LONG).show();
			}
		});
		
		showmsg(this, shareref.getString("username","000") 
				+ shareref.getString("username","000"));
		
	}
	
	public static void showmsg(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
}
