package com.example.tu_app_1.myadapter;

import java.util.ArrayList;

import com.example.tu_app_1.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Adapter1 extends BaseAdapter{ // tuy chinh view thanh phan 
	Context context;
	ArrayList<String>data;
	
	public Adapter1(Context context, ArrayList<String>data) {
		this.data = data;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int index) {
		// TODO Auto-generated method stub
		return data.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater lif = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		view = lif.inflate(R.layout.listviewitem, null, false); 
		
		TextView tv = (TextView) view.findViewById(R.id.tv1); 
		tv.setText(data.get(position));
		
		return view;
	}

}
